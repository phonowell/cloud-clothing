#============
#require
#============
exec = (require 'child_process').exec

gulp = require 'gulp'
gutil = require 'gulp-util'
watch = require 'gulp-watch'
plumber = require 'gulp-plumber'
replace = require 'gulp-replace'
clean = require 'gulp-clean'
ignore = require 'gulp-ignore'
concat = require 'gulp-concat'
rename = require 'gulp-rename'

uglify = require 'gulp-uglify'

jade = require 'gulp-jade'
coffee = require 'gulp-coffee'
stylus = require 'gulp-stylus'
cson = require 'gulp-cson'

lint = require 'gulp-coffeelint'

#============
#error
#============
#uncaughtException
process.on 'uncaughtException', (err) -> log err.stack

#============
#function
#============
#log
log = console.log

#============
#param
#============
#base
base = process.cwd()

#============
#task
#============

#watch
gulp.task 'watch', ->

  #lint
  watch ['./**/*.coffee', '!./node_modules/**']
  .pipe lint()
  .pipe lint.reporter()

  #index
  watch './source/**/*.jade', ->
    gulp.src 'index.jade'
    .pipe plumber()
    .pipe jade()
    .pipe gulp.dest './'

  #style
  watch './source/style/import/**/*.styl', ->
    gulp.src './source/style/style.styl'
    .pipe plumber()
    .pipe stylus compress: true
    .pipe gulp.dest './source/style/'

    #delay to clean .css files
    setTimeout ->
      gulp.src './source/style/import/**/*.css'
      .pipe clean()
    , 1e3

  #uglify
  watch ['./**/*.js', '!./node_modules/**']
  .pipe plumber()
  .pipe uglify()
  .pipe rename suffix: '.min'
  .pipe gulp.dest './'

  #remove .min.min.js
  setInterval ->
    gulp.src ['./**/*.min.min.js', '!./node_modules/**']
    .pipe clean()
  , 30e5

#lint
gulp.task 'lint', ->
  gulp.src ['./**/*.coffee', '!./node_modules/**']
  .pipe lint()
  .pipe lint.reporter()

#build
gulp.task 'build', ->
  #stylus
  gulp.src './source/style/style.styl'
  .pipe plumber()
  .pipe stylus compress: true
  .pipe gulp.dest './build/style/'

  #image
  gulp.src ['./source/style/image/*.jpg', './source/style/image/*.png']
  .pipe plumber()
  .pipe gulp.dest './build/style/image/'

  #jade
  gulp.src './index.jade'
  .pipe plumber()
  .pipe jade()
  .pipe replace /source\//g, ''
  .pipe gulp.dest './build/'

#clear
gulp.task 'clean', ->
  gulp.src ['./**/*.min.min.js', '!./node_modules/**']
  .pipe clean()