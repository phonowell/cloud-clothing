###
  core.coffee
###

#================
#params
#================
system = window.system = {}
#================

#================
#function
#================
#selector
$$ = window.$$ = (selector) ->
  h = system.handle or= {}
  if s = selector
    h[s] or h[s] = $ s
  else h

#log
$.log = (param) -> console?.log? param
#================

#================
#gui
#================
#gui
$.gui = {}

#gui.set
$.gui.set = (param) ->
  $.log param
  switch param
    #base
    when 'base'
      $$('#stage').load 'source/interface/base.html'
#================